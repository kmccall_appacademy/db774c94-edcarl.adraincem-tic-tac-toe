class HumanPlayer
  attr_accessor :mark, :name

  def initialize(name)
    @name = name
    @mark = mark
  end

  def display(board)
    puts " #{board[[0, 0]]}  | #{board[[0, 1]]}  | #{board[[0, 2]]}  "
    puts "_____________"
    puts " #{board[[1, 0]]}  | #{board[[1, 1]]}  | #{board[[1, 2]]}  "
    puts "_____________"
    puts " #{board[[2, 0]]}  | #{board[[2, 1]]}  | #{board[[2, 2]]}  "
  end

  def get_move
    puts "#{name}: Where is your next move? (row, col)"
    input = gets.chomp
    pos = input.split(", ")
    pos.each_with_index do |el, idx|
      pos[idx] = el.to_i
    end
    pos
  end

end
