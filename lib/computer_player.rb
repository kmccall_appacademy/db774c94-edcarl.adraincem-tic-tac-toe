class ComputerPlayer
  attr_accessor :mark, :name
  attr_reader :board

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move
    moves = []
    3.times do |row|
      3.times do |col|
        pos = [row, col]
        moves << pos if board[pos].nil?
      end
    end
    moves.each { |pos| return pos if winning_move?(pos) }

    moves.sample
  end

  def winning_move?(move)
    row, col = move
    board.grid[row][col] = mark
    if board.winner
      board.grid[row][col] = nil
      true
    else
      board.grid[row][col] = nil
      false
    end
  end

end
