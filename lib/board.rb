require 'byebug'

class Board
  attr_accessor :grid

  def initialize(grid = [])
    if grid.empty?
      @grid = Array.new(3) { Array.new(3) }
    else
      @grid = grid
    end
  end

  def place_mark(pos, mark)
    row, col = pos
    grid[row][col] = mark
  end

  def empty?(pos)
    row, col = pos
    return true if grid[row][col].nil?
    false
  end

  def cols
    cols = []
    3.times do |col|
      column = []
      3.times { |row| column << grid[row][col] }
      cols << column
    end
    cols
  end

  def diags
    diag = []
    right = []
    left = []
    count = 0
    2.downto(0) do |pos|
      left << grid[pos][pos]
      right << grid[count][pos]
      count += 1
    end
    diag << left << right
    diag
  end

  def winner
    (grid + cols + diags).each do |marks|
      return :X if marks == [:X, :X, :X]
      return :O if marks == [:O, :O, :O]
    end
    nil
  end

  def over?
    return true if winner == :X || winner == :O
    return true if grid.flatten.none? { |el| el == nil }
    false
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

end
