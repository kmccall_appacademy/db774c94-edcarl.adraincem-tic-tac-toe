require 'byebug'

require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :board, :player_one, :player_two, :current_player

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    player_one.mark = :X
    player_two.mark = :O
    @board = Board.new
    @current_player = player_one
  end

  def switch_players!
    if current_player == player_one
      self.current_player = player_two
    else
      self.current_player = player_one
    end
  end

  def play_turn
    current_player.display(board)
    move = current_player.get_move
    until board.empty?(move)
      print "Try again "
      move = current_player.get_move
    end
    board.place_mark(move, current_player.mark)
    switch_players!
  end

  def play
    until board.over?
      play_turn
    end
    winner
  end

  def winner
    if board.winner == nil
      puts "TIE GAME!"
    else
      puts "#{board.winner} wins!"
    end
    current_player.display(board)
  end

end

if __FILE__ == $PROGRAM_NAME
  a = HumanPlayer.new("Player A")
  b = ComputerPlayer.new("Player B")
  game = Game.new(a, b)
  game.play
end
